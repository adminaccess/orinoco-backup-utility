﻿// Version 1.2
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using OrinocoAccess;

namespace OrinocoDatabase
{
    public class OrinocoDB
    {
        // Points to the path of the database file
        private string mDbFile;

        public List<OrinocoUser> Users;

        // Default constructor
        public OrinocoDB(string dbFile)
        {
            mDbFile = dbFile;
            Users = new List<OrinocoUser>();
        }

        // Destructor
        ~OrinocoDB()
        {
            mDbFile = "";
            Users.Clear();
        }

        /// <summary>
        /// Get the path of the current database file.
        /// </summary>
        /// <returns>A string with the file path.</returns>
        public string GetDatabasePath()
        {
            return mDbFile;
        }

        /// <summary>
        /// Saves the list of users to the given database.
        /// </summary>
        public void Save()
        {
            XmlSerializer writer = new XmlSerializer(typeof(List<OrinocoUser>));
            FileStream dbFile = null;

            try
            {
                dbFile = File.Create(mDbFile);
                writer.Serialize(dbFile, Users);
            }
            catch (Exception ex)
            {
                if (dbFile != null)
                    dbFile.Close();
                throw new Exception(ex.Message);
            }

            dbFile.Close();
        }

        /// <summary>
        /// Loads the given database file into the Users list.
        /// </summary>
        public void Load()
        {
            XmlSerializer reader = new XmlSerializer(typeof(List<OrinocoUser>));
            StreamReader dbFile = null;

            try
            {
                dbFile = new StreamReader(mDbFile);
                Users = (List<OrinocoUser>)reader.Deserialize(dbFile);
            }
            catch (Exception ex)
            {
                if (dbFile != null)
                    dbFile.Close();
                throw new Exception(ex.Message);
            }

            dbFile.Close();
        }

        /// <summary>
        /// Get a user object by employee ID.
        /// </summary>
        /// <param name="employeeID">The employee ID</param>
        /// <returns>OrinocoUser object</returns>
        public OrinocoUser GetUserById(uint employeeID)
        {
            foreach (OrinocoUser user in Users)
            {
                if (user.EmployeeID == employeeID)
                {
                    return user;
                }
            }

            // The user was not fount, so return null
            return null;
        }

        /// <summary>
        /// Get a user object by the user's name
        /// </summary>
        /// <param name="username">The username of the user.</param>
        /// <returns>OrinocoUser object.</returns>
        public OrinocoUser GetUserByName(string username)
        {
            foreach (OrinocoUser user in Users)
            {
                if (user.Username == username)
                {
                    return user;
                }
            }

            // The user was not found, so return null
            return null;
        }
    }
}
